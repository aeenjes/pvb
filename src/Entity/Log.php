<?php

namespace App\Entity;

use App\Repository\LogRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LogRepository::class)
 */
class Log
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json")
     */
    private $typeTable = [];

    /**
     * @ORM\Column(type="text")
     */
    private $generalLog;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="logs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lastEditUser;

    /**
     * @ORM\Column(type="datetime")
     */
    private $lastEditDate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeTable(): ?array
    {
        return $this->typeTable;
    }

    public function setTypeTable(array $typeTable): self
    {
        $this->typeTable = $typeTable;

        return $this;
    }

    public function getGeneralLog(): ?string
    {
        return $this->generalLog;
    }

    public function setGeneralLog(string $generalLog): self
    {
        $this->generalLog = $generalLog;

        return $this;
    }

    public function getLastEditUser(): ?User
    {
        return $this->lastEditUser;
    }

    public function setLastEditUser(?User $lastEditUser): self
    {
        $this->lastEditUser = $lastEditUser;

        return $this;
    }

    public function getLastEditDate(): ?\DateTimeInterface
    {
        return $this->lastEditDate;
    }

    public function setLastEditDate(\DateTimeInterface $lastEditDate): self
    {
        $this->lastEditDate = $lastEditDate;

        return $this;
    }
}
