<?php

namespace App\Entity;

use App\Repository\LandRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LandRepository::class)
 */
class Land
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $iso;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $naam;

    /**
     * @ORM\OneToOne(targetEntity=Woonlandfactor::class, mappedBy="landId", cascade={"persist", "remove"})
     */
    private $woonlandfactor;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIso(): ?string
    {
        return $this->iso;
    }

    public function setIso(string $iso): self
    {
        $this->iso = $iso;

        return $this;
    }

    public function getNaam(): ?string
    {
        return $this->naam;
    }

    public function setNaam(string $naam): self
    {
        $this->naam = $naam;

        return $this;
    }

    public function getWoonlandfactor(): ?Woonlandfactor
    {
        return $this->woonlandfactor;
    }

    public function setWoonlandfactor(?Woonlandfactor $woonlandfactor): self
    {
        $this->woonlandfactor = $woonlandfactor;

        // set (or unset) the owning side of the relation if necessary
        $newLandId = null === $woonlandfactor ? null : $this;
        if ($woonlandfactor->getLandId() !== $newLandId) {
            $woonlandfactor->setLandId($newLandId);
        }

        return $this;
    }
}
