<?php

namespace App\Entity;

use App\Repository\FormulierDefaultsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FormulierDefaultsRepository::class)
 */
class FormulierDefaults
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $jaar;

    /**
     * @ORM\Column(type="integer")
     */
    private $versie;

    /**
     * @ORM\Column(type="float")
     */
    private $rekenfactor1;

    /**
     * @ORM\Column(type="float")
     */
    private $rekenfactor2;

    /**
     * @ORM\Column(type="integer")
     */
    private $maxVermogen;

    /**
     * @ORM\Column(type="integer")
     */
    private $standaardpremie;

    /**
     * @ORM\Column(type="integer")
     */
    private $maxToetsingsinkomen;

    /**
     * @ORM\Column(type="integer")
     */
    private $drempelinkomen;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJaar(): ?\DateTimeInterface
    {
        return $this->jaar;
    }

    public function setJaar(\DateTimeInterface $jaar): self
    {
        $this->jaar = $jaar;

        return $this;
    }

    public function getVersie(): ?int
    {
        return $this->versie;
    }

    public function setVersie(int $versie): self
    {
        $this->versie = $versie;

        return $this;
    }

    public function getRekenfactor1(): ?float
    {
        return $this->rekenfactor1;
    }

    public function setRekenfactor1(float $rekenfactor1): self
    {
        $this->rekenfactor1 = $rekenfactor1;

        return $this;
    }

    public function getRekenfactor2(): ?float
    {
        return $this->rekenfactor2;
    }

    public function setRekenfactor2(float $rekenfactor2): self
    {
        $this->rekenfactor2 = $rekenfactor2;

        return $this;
    }

    public function getMaxVermogen(): ?int
    {
        return $this->maxVermogen;
    }

    public function setMaxVermogen(int $maxVermogen): self
    {
        $this->maxVermogen = $maxVermogen;

        return $this;
    }

    public function getStandaardpremie(): ?int
    {
        return $this->standaardpremie;
    }

    public function setStandaardpremie(int $standaardpremie): self
    {
        $this->standaardpremie = $standaardpremie;

        return $this;
    }

    public function getMaxToetsingsinkomen(): ?int
    {
        return $this->maxToetsingsinkomen;
    }

    public function setMaxToetsingsinkomen(int $maxToetsingsinkomen): self
    {
        $this->maxToetsingsinkomen = $maxToetsingsinkomen;

        return $this;
    }

    public function getDrempelinkomen(): ?int
    {
        return $this->drempelinkomen;
    }

    public function setDrempelinkomen(int $drempelinkomen): self
    {
        $this->drempelinkomen = $drempelinkomen;

        return $this;
    }
}
