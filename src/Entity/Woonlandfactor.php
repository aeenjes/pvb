<?php

namespace App\Entity;

use App\Repository\WoonlandfactorRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WoonlandfactorRepository::class)
 */
class Woonlandfactor
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=Land::class, inversedBy="woonlandfactor", cascade={"persist", "remove"})
     */
    private $landId;

    /**
     * @ORM\Column(type="float")
     */
    private $factor;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLandId(): ?Land
    {
        return $this->landId;
    }

    public function setLandId(?Land $landId): self
    {
        $this->landId = $landId;

        return $this;
    }

    public function getFactor(): ?float
    {
        return $this->factor;
    }

    public function setFactor(float $factor): self
    {
        $this->factor = $factor;

        return $this;
    }
}
