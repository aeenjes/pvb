<?php

namespace App\Repository;

use App\Entity\FormulierDefaults;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FormulierDefaults|null find($id, $lockMode = null, $lockVersion = null)
 * @method FormulierDefaults|null findOneBy(array $criteria, array $orderBy = null)
 * @method FormulierDefaults[]    findAll()
 * @method FormulierDefaults[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormulierDefaultsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FormulierDefaults::class);
    }

    // /**
    //  * @return FormulierDefaults[] Returns an array of FormulierDefaults objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FormulierDefaults
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
