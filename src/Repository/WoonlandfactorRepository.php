<?php

namespace App\Repository;

use App\Entity\Woonlandfactor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Woonlandfactor|null find($id, $lockMode = null, $lockVersion = null)
 * @method Woonlandfactor|null findOneBy(array $criteria, array $orderBy = null)
 * @method Woonlandfactor[]    findAll()
 * @method Woonlandfactor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WoonlandfactorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Woonlandfactor::class);
    }

    // /**
    //  * @return Woonlandfactor[] Returns an array of Woonlandfactor objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Woonlandfactor
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
