<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200605075020 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE formulier_defaults (id INT AUTO_INCREMENT NOT NULL, jaar DATE NOT NULL, versie INT NOT NULL, rekenfactor1 DOUBLE PRECISION NOT NULL, rekenfactor2 DOUBLE PRECISION NOT NULL, max_vermogen INT NOT NULL, standaardpremie INT NOT NULL, max_toetsingsinkomen INT NOT NULL, drempelinkomen INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE land (id INT AUTO_INCREMENT NOT NULL, iso VARCHAR(2) NOT NULL, naam VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE log (id INT AUTO_INCREMENT NOT NULL, last_edit_user_id INT NOT NULL, type_table JSON NOT NULL, general_log LONGTEXT NOT NULL, last_edit_date DATETIME NOT NULL, INDEX IDX_8F3F68C51466127B (last_edit_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, username VARCHAR(30) NOT NULL, reset_hash VARCHAR(40) NOT NULL, last_login DATETIME DEFAULT NULL, created_on DATETIME NOT NULL, active SMALLINT NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE woonlandfactor (id INT AUTO_INCREMENT NOT NULL, land_id_id INT DEFAULT NULL, factor DOUBLE PRECISION NOT NULL, UNIQUE INDEX UNIQ_495FC7AB9D6F1078 (land_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE log ADD CONSTRAINT FK_8F3F68C51466127B FOREIGN KEY (last_edit_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE woonlandfactor ADD CONSTRAINT FK_495FC7AB9D6F1078 FOREIGN KEY (land_id_id) REFERENCES land (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE woonlandfactor DROP FOREIGN KEY FK_495FC7AB9D6F1078');
        $this->addSql('ALTER TABLE log DROP FOREIGN KEY FK_8F3F68C51466127B');
        $this->addSql('DROP TABLE formulier_defaults');
        $this->addSql('DROP TABLE land');
        $this->addSql('DROP TABLE log');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE woonlandfactor');
    }
}
